\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}A Topic Modeling Based Similarity Measure}{2}
\contentsline {subsection}{\numberline {2.1}Latent Dirichlet Allocation}{3}
\contentsline {subsection}{\numberline {2.2}Kullback-Leibler Divergence}{3}
\contentsline {section}{\numberline {3}Implementation}{4}
\contentsline {subsection}{\numberline {3.1}Training the Topic Model}{5}
\contentsline {subsection}{\numberline {3.2}Indexing the Documents}{6}
\contentsline {subsection}{\numberline {3.3}A Function Score Query with a Custom Similarity Score}{8}
\contentsline {subsection}{\numberline {3.4}Speed Up Using Prefiltering}{9}
\contentsline {section}{\numberline {4}Dependencies}{11}
\contentsline {section}{\numberline {5}Build Instructions}{11}
\contentsline {section}{\numberline {6}Usage Instructions}{12}
\contentsline {subsection}{\numberline {6.1}Retrieve Documents According to Traditional MLT}{12}
\contentsline {subsection}{\numberline {6.2}Retrieve Documents with Similar Topic Distributions}{12}
\contentsline {subsection}{\numberline {6.3}View Comparing Results of Classic and Modified More Like This}{12}
\contentsline {subsection}{\numberline {6.4}Retrieving Documents using the Speedy Query}{13}
\contentsline {subsection}{\numberline {6.5}Indexing a new Document}{13}
\contentsline {subsection}{\numberline {6.6}Running the tests}{13}
\contentsline {section}{\numberline {7}Documentation}{13}
\contentsline {section}{\numberline {8}Conclusion}{14}
\contentsline {section}{\numberline {A}Appendix}{15}
