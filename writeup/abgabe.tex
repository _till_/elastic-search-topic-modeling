
%%%
%%% HEADER
%%%
 

\documentclass[egregdoesnotlikesansseriftitles]{article}
\usepackage[utf8]{inputenc}
\usepackage{bera}% optional: just to have a nice mono-spaced font
\usepackage{listings}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{pdfpages}

\colorlet{punct}{red!60!black}
\definecolor{background}{HTML}{EEEEEE}
\definecolor{delim}{RGB}{20,105,176}
\colorlet{numb}{magenta!60!black}

\lstdefinelanguage{json}{
    basicstyle=\normalfont\ttfamily,
    numbers=left,
    numberstyle=\scriptsize,
    stepnumber=1,
    numbersep=8pt,
    showstringspaces=false,
    breaklines=true,
    frame=lines,
    backgroundcolor=\color{background},
    literate=
     *{0}{{{\color{numb}0}}}{1}
      {1}{{{\color{numb}1}}}{1}
      {2}{{{\color{numb}2}}}{1}
      {3}{{{\color{numb}3}}}{1}
      {4}{{{\color{numb}4}}}{1}
      {5}{{{\color{numb}5}}}{1}
      {6}{{{\color{numb}6}}}{1}
      {7}{{{\color{numb}7}}}{1}
      {8}{{{\color{numb}8}}}{1}
      {9}{{{\color{numb}9}}}{1}
      {:}{{{\color{punct}{:}}}}{1}
      {,}{{{\color{punct}{,}}}}{1}
      {\{}{{{\color{delim}{\{}}}}{1}
      {\}}{{{\color{delim}{\}}}}}{1}
      {[}{{{\color{delim}{[}}}}{1}
      {]}{{{\color{delim}{]}}}}{1},
}

%\usepackage[latin1]{inputenc}
 
\title{LDA Based More Like This}

\author{Till Ilić}
\date{31.03.2019} 
 
\begin{document}
\maketitle
\vfill
\tableofcontents
\vfill
\newpage
\section{Introduction}

Goal of this implementation is to provide a functionality, similar to the \textit{More Like This} \cite{MLT} feature provided in Elastic Search. 
More Like this provides suggestions of documents that are similar to a document \textit{d} (or a set of documents) of interest. A similarity score is computed with all the documents \textit{d'} in the corpus and a ranking of the documents, based on their similarity to \textit{d} is returned. The scoring mechanism applied is as follows: 

\begin{enumerate}
\item extract text from the input document and analyze it
\item select \textit{k} words with the highest TFIDF-scores from the text
\item form a disjunctive query over the extracted words
\item return a ranking of the documents, according to their score 
\end{enumerate}  

Using tfidf features gives high weight to words that are frequent in a document but not frequent in the corpus overall.
This leads to documents being suggested that have the same infrequent (corpus-wide) words.

The article describing a movie \textit{Starwars episode 1} will then for example suggest the article about \textit{Starwars episode 2} because the words Joda, light saber etc. appear in both articles, but are infrequent in the corpus overall.

In many use cases this is the wished for behavior. There might however also be cases where it is instead preferable to get suggestions that are more varied, but still belong broadly to the same topic.

This could for example be useful when a more exploratory behavior is wished by the user. 

In the example used above a suggestion strategy differing from the standard MLT approach could recommend articles that may not be about starwars, but still about other movies in general.

With this goal in mind a similarity measure can be defined, which gives a high score not just to documents where the same words appear, but instead to documents which discuss similar \textit{topics}.

The proposed approach is introduced in the following sections.

\section{A Topic Modeling Based Similarity Measure}

Topic Models are unsupervised models that aims to uncover the underlying topic distribution in a corpus of text.
They search for topics in the corpus and assign a distribution of topics to each of the documents, which states how much of the document belongs to each of the topics. 

One of the most popular topic modeling approaches used in practice is \textit{Latent Dirichlet Allocation} (LDA).

\subsection{Latent Dirichlet Allocation}

Latent Dirichlet Allocation (LDA) is a generative model taking the following view of the document creation process. 


\begin{itemize}

\item for each topic $t$:
\begin{itemize}
\item draw a categorical word distribution $\varphi_t$ according to a \\ dirichlet prior $\beta$
\end{itemize}

\item for each document d:
\begin{itemize}
\item draw a categorical topic distribution $\theta_d$ according to a \\ dirichlet prior $\alpha$
\item for each word in d:
\begin{itemize}
\item draw a topic $z$ according to $\theta_d$
\item draw word according to $\varphi_z$, the categorical word distribution of topic $z$ 
\end{itemize}
\end{itemize}
\end{itemize}

The corresponding graphical model is depicted in figure     
\ref{fig:lda_plate_notation}.
\begin{figure}
\centering
\includegraphics[scale=0.5]{lda_plate_notation.png}
\caption{Plate Notation of LDA Topic Model, figure taken from \protect\cite{LDAP}}
\label{fig:lda_plate_notation}
\end{figure}
The number of topics has to be set by the user.
LDA is a bag-of-words model, not taking into account any sequential information.
After training the model it can be used to calculate the topic distribution for a new document. The user has to provide a document, transformed term-frequency-vector and the LDA-model returns how much each of the learned topics is present in the document.

To use the topic distribution for scoring documents a similarity measure over probability distributions has to be used. The \textit{Kullback-Leibler Divergence} is such a measure. It is introduced in the following section.

\subsection{Kullback-Leibler Divergence}
\label{sec:kld}
The Kullback-Leibler divergence (KL-divergence) measures how well a probability distribution $Q$ approximates another probability distribution $P$. 
A high KL-divergence $D_{KL}(P||Q)$ means that $Q$ is a bad approximation of $P$ whereas a low KL-divergence means that it is a good approximation.
For discrete probability distributions $P$ and $Q$ it is defined as follows:
$$
D_{KL}(P||Q) = \sum_{x \epsilon X}P(x)\log\frac{Q(x)}{P(x)}
$$
The KL-divergence is non-negative and becomes zero in the case where $P$ and $Q$ are the same distribution.

The Kullback-Leibler divergence is asymmetric ($D_{KL}(P||Q) = D_{KL}(Q||P)$ does not hold) and is therefore not a distance measure.

The asymmetry of the KL-divergence is useful in the case of comparing topic distributions as it gives more weight to deviations in topics that are considered significant in $P$.
To illustrate the point one can consider a toy example: 
Let's assume we have a topic model with five different topics.
Let Document 1 have a topic distribution $P$ that gives weight 0.6 to topic 0 and weight 0.1 to all other topics. Let Document 2 have a topic distribution $Q$ that gives weight 0.2 to all topics. The topic distributions are illustrated in figure \ref{fig:kl_divergence_example}.
Now $D_{KL}(P||Q)$ (the measure of how well $Q$ approximates $P$) becomes 0.382 and $D_{KL}(Q||P)$ (the measure of how well $P$ approximates $Q$) becomes 0.335.
This means that $P$ is a better approximation of $Q$ than the other way around. The high difference in the first topic is weighted more when we are looking to approximate $P$ since $P$ gives a lot of weight to topic 0. On the other hand $P$ and $Q$ have both got similar values for topics 1 to 5 and since these topics together make up a large part of Document 2 $P$ is considered a better approximation to $Q$.

To use the KL-divergence in a similarity score one can take it's inverse.
A small positive value $\alpha$ needs to be added to the denominator in order to avoid division by zero in case of equal topic distributions.
The final similarity score of two documents with the topic distributions $P$ and $Q$ will then look like this:
$$sim(P, Q) = \frac{1}{D_{KL}(P||Q) + \alpha}$$
As mentioned, this similarity score will give a high score if $Q$ is a good approximation of $P$, with a lot of weight given to differences in topics that are considered important in the document with probability distribution $P$.
\begin{figure}
\centering
 \makebox[\textwidth][c]{\includegraphics[width=1.2\textwidth]{KL_example_topic_docs.png}}
\caption{Topic Distributions of 2 Documents}
\label{fig:kl_divergence_example}
\end{figure}

\section{Implementation}
The main components of the implementation are:
\begin{enumerate}
\item a server providing a RESTful API
\item a script which trains an LDA topic model on the Wikipedia dump and prepares the artiles for indexing
\item a build script which starts and sets up elasticsearch and indexes the documents
\item a test suite
\end{enumerate}
Documentation of the written code can be found in the \textit{documentation} directory.

The following sections discuss decisions made during the implementation process.
Section \ref{sec:topictraining} is about the training of a topic model. Section \ref{sec:index} continues with decisions made regarding the indexing process. The following sections describe how the custom similarity score was implemented and how using this custom similarity score can be sped up by filtering out unpromising candidates beforehand.

\subsection{Training the Topic Model}
\label{sec:topictraining}
As a first step the articles from a Wikipedia dump were extracted using the open source software \textit{Wikipedia Extractor} \cite{WEX}.

An LDA topic model was trained on the first 50.000 articles using functions provided by the \textit{scikit-learn} libary \cite{SKL}. I trained a model with 25 different topics. The resulting topics look reasonable (see table \ref{tab:topics} for an extract and the \textit{topics.txt} file for all topics, described by their 10 most likely words), but different numbers of topics can be tried out in order to maximize performance.
Articles with less than 250 words were thrown out as I did not consider them insightful.

All words appearing less than twice in the overall corpus or in more than 90\% of the documents were disregarded when building the LDA model. Words contained in a standard list of english stopwords provided by \textit{sklearn} were also removed before training. Of the remaining words, the most frequent 10000 were used to train the model. The reasoning behind throwing out infrequent and very frequent words is that they do not provide any useful information about topics. Words that appear only once cannot be a strong indicator for a topic and words that appear extremely frequently must appear in different topics. Limiting the vocabulary to 1000 words was done in order to speed up the training of the model. 

\begin{table}
\begin{center}
\begin{tabular}{ccccc}
\textbf{Topic 0} & \textbf{Topic 1} & \textbf{Topic 11} & \textbf{Topic 15} & \textbf{Topic 17} \\ 
\hline
music & team  & city  & town  & king  \\ 
musical & world  & american  & area  & england  \\ 
rock   & league  & new  & river  & john  \\ 
sound & won  & states  & county  & london  \\ 
jazz  & season  & texas  & park  & house  \\ 
new  & club  & state  & north  & british  \\ 
orchestra  & game  & united  & south  & royal  \\ 
early  & football  & railroad  & west  & william  \\ 
blues& time  & san  & east  & henry  \\ 
recorded & games & california & lake & castle \\
\hline
\end{tabular}
\label{tab:topics}
\end{center}
\caption{The 10 most likely words for 5 selected topics, found by LDA in the Wikipedia corpus}
\end{table}

\subsection{Indexing the Documents}
\label{sec:index}

To allow for a topic based MLT query the topic information of each document needs to be stored. Elasticsearch provides the type object which can hold a structured data. A mapping for the annotated articles using the object type could look like this.

\begin{lstlisting}[language=json,firstnumber=1]
{
    "article": {
        "properties": {
            "url"   : {"type": "text"},
            "id"    : {"type": "text"},
            "text"  : {"type": "text"},
            "title" : {"type": "text"},
            "topics": {
                "properties": {
                    "topic_0"  : {"type" : "float"},
                    ...
                    "topic_24" : {"type" : "float"}
                }
            }
        }
    }
} 
\end{lstlisting}

This would provide a cleanly structured mapping where the whole topic distribution is grouped under the \textit{topics} heading. 
Unfortunately the topic distribution could not be accessed easily in a scripted query, as is mentioned in \cite{ESMAP}.
There it says:
\textit{It’s important to understand the difference between doc['my\_field'].value and params['\_source']['my\_field']. The first, using the doc keyword, will cause the terms for that field to be loaded to memory (cached), which will result in faster execution, but more memory consumption. \textbf{Also, the doc[...] notation only allows for simple valued fields (you can’t return a json object from it)}} (highlighting added by me). This means that the individual topic values cannot be accessed using the doc[...] notation. Using params['\_source']['my\_field'] notation leads to a very slow execution time of the query and was approximately 7 times slower in my experiments than using the doc[...] notation in combination with the flat mapping discussed below.

\begin{lstlisting}[language=json,firstnumber=1,label=list:mapping]
{
    "article": {
        "properties": {
            "url"   : {"type": "text"},
            "id"    : {"type": "text"},
            "text"  : {"type": "text"},
            "title" : {"type": "text"},
            "topic_0"  : {"type" : "float"},
            ...
            "topic_24" : {"type" : "float"}
        }
    }
} 
\end{lstlisting}
This flat mapping loses the elegance of the mapping shown further above. However the significant speedup when using this mapping outweighs the disadvantages.

The initial indexing of the documents is done when running the build script. The topic distributions for each document have already been calculated using the \textit{prepare\_initial\_index.py} script and added to each document.

Indexing new documents can be done by posting to the \textit{/mlt/add} endpoint provided by the flask server and is explained in detail in section \ref{sec:indexusage}.

\subsection{A Function Score Query with a Custom Similarity Score}
\label{sec:modified}
The choice of similarity score is discussed in detail in section \ref{sec:kld}. The definition of the chosen similarity score is repeated here for convenience:
$$sim(P, Q) = \frac{1}{D_{KL}(P||Q) + \alpha}$$
Elasticsearch provides it's own scripting language for use in scripted queries called \textit{painless}.

To retrieve documents using a scripted similarity score the \textit{Function Score Query} can be used in combination with \textit{script\_score}\cite{FSSS}.
In this case the function score query wraps another query which is used to retrieve documents to be scored with the scripted score. In the simplest case this query is a \textit{match\_all} query which retrieves all documents and the script score is calculated for each of them.

An abbreviated version of this query is shown below in listing \ref{list:modified}.
\begin{lstlisting}[language=json,firstnumber=1, caption=A scripted query for a document with a topic distribution over only two topics, label=list:modified]
{
  "query": {
    "function_score": {
      "query": {
        "match_all": {}
      },
      "script_score": {
        "script": {
          "source": "return 1 / 
                    (params.topic_0 * Math.log(params.topic_0 / doc['topic_0'].value) + params.topic_1 * Math.log(params.topic_1 / doc['topic_1'].value) + 0.1);",
          "params": {
            "topic_0": 0.8,
            "topic_1": 0.2
          }
        }
      },
       "boost_mode": "replace"
    }
  }
}
\end{lstlisting}
The similarity score is provided in the \textit{source} parameter of the script, written in \textit{painless}. Here an $\alpha$ of 0.1 was used in order to avoid division by zero for two equal topic distributions. The example uses a topic distribution with only two topics for brevity. The topic distribution of the document of interest is provided in the \textit{params}. 
This query calculates the similarity score on all document, since a \textit{match\_all} query is used. For larger corpora this can be prohibitively expensive. A method to preselect the documents for which to calculate the similarity score is described in the next section.  

\subsection{Speed Up Using Prefiltering}
\label{sec:speedy}
When using the standard script query introduced in the previous section an expensive calculation has to be made, comparing the document of interest to each of the document in the corpus.

Inspection of the corpus and the trained topic model shows that most documents only have a significant weight for a small number of documents.
A histogram showing how many documents have a most significant topic with a weight \textit{w} for different weights, see figure \ref{fig:mostsig}.

\begin{figure}
\center
\includegraphics[scale=0.45]{percentage_highest_topic.png}
\caption{Number of documents having a most significant topic with a specified weight}
\label{fig:mostsig}
\end{figure}

All documents have negligible weights for most of the topics. A simple heuristic can therefore be formulated:
When searching for documents similar to document $d$, all documents that do not have a significant weight for the most significant topic of $d$ can be ruled out prior to calculating the similarity score.
The needed significance can be specified by a threshold. When using a conservative threshold of 5\% in most cases (depending on the most significant topic) more than 80\% of the documents can be disregarded before the scripted score needs to be calculated. The exact percentage of documents that can be disregarded as a function of the most significant topic can be seen in the figures in appendix \ref{appendix}.

To implement this heuristic in elasticsearch the \textit{match\_all} query in the function score above (listing \ref{list:modified}) can be replaced by a range query. The resulting query is shown in listing \ref{list:speedy}

\begin{lstlisting}[language=json,firstnumber=1, caption=Function score using range query to filter out dissimilar documents before calculating the script score, label=list:speedy]
{
  "query": {
    "function_score": {
      "boost_mode": "replace",
      "query": {
        "range": {
          "topic_1": {
            "gte": 0.05
          }
        }
      },
      "script_score": {
        "script": {
          "source": "return 1/(params.topic_0 * Math.log(params.topic_0 / doc['topic_0'].value)+params.topic_1 * Math.log(params.topic_1 / doc['topic_1'].value));",
          "params": {
            "topic_1": 0.8,
            "topic_0": 0.2
          }
        }
      }
    }
  }
}

\end{lstlisting}

By cheaply comparing two numbers for the most significant topic of $d$ the majority of documents can be disregarded without having to calculate the similarity measure for them.
This leads to significant speed up in query execution. To measure this speed up I wrote a script that repeats the following process for a large number of times and averages the times it took:

\begin{enumerate}
\item clear the elasticsearch cache
\item perform a function\_score query without using prefilterting (match\_all query) and record the time
\item clear the elasticsearch cache
\item perform a function\_score query using prefilterting (range query) and record the time
\end{enumerate}

The script is provided in the \textit{time\_queries.py} file. When repeating the process 1000 times it returns an average time of 203.788ms when using the match\_all query and an average time of 27.768ms when using the range query.

\section{Dependencies}
The following list provides an overview over the dependencies used in this project.

\begin{itemize}
\item the \textit{flask} was used to develop the server
\item \textit{numpy} was used for all calculations 
\item the \textit{requests} and \textit{elasticsearch-py} libraries were used for handling requests to the elasticsearch and flask servers
\item the \textit{pandas} library was used mostly during exploratory data analysis
\item documentation of the code was generated using \textit{pdoc}
\item \textit{pytest} was used as a test framework 
\item \textit{sklearn} was used for training the topic model
\item the plots found in this writeup were generated using \textit{matplotlib}
\end{itemize}

\section{Build Instructions}
This project can be built by running the buildscript (\textit{bash build.sh}).
To change the port where elasticsearch runs, change it in the docker-compose file and also in build script. 
Running the build script will set in motion the following process:
\begin{enumerate}
\item docker containers running elasticsearch and cerebro are started
\item any previous existing index of the specified name (the default name is \textit{topic-docs}) is deleted
\item an index with the provided name will be created
\item the corpus of 50.000 Wikipedia articles, annotated with their topic information, is indexed
\item an explicit mapping (see listing \ref{list:mapping}) is posted
\item a python virtual environment is created and all needed dependencies are installed into it
\item the \textit{flask} server, providing all functionalities, is started
\end{enumerate}


\section{Usage Instructions}
The implemented RESTful API provides multiple functionalities. They are described in the following sections. 
\subsection{Retrieve Documents According to Traditional MLT}
Results of the traditional more like this query are available using the endpoint \textit{mlt/classic/id}.
GET Requests to this address will return documents considered similar by the original more like this similarity for the document with the specified \textit{id}.
\\\\
An example query (using curl) could look like this:\\
\textit{curl -XGET http://localhost:5000/mlt/classic/123}  

\subsection{Retrieve Documents with Similar Topic Distributions}

To retrieve documents similar according to the similarity score defined in section \ref{sec:kld} the endpoint \textit{mlt/modified/id} is used.
This will send a function score query as shown in section \ref{sec:modified}.
\\\\
An example query (using curl) could look like this:\\
\textit{curl -XGET http://localhost:5000/mlt/modified/123}  

\subsection{View Comparing Results of Classic and Modified More Like This}

A view comparing the results of the classic and modified more like this queries is available under the  \textit{mlt/compare/id} endpoint. 

Here the 5 best suggestions of each query are compared in tabular form.
It is intended for a visual exploration and comparison of the different results. As this renders an html view, a web browser should be used.
\\\\
To use this endpoint visit in a browser e.g.\\
\textit{http://localhost:5000/mlt/compare/123}  
\\\\
If everything works correctly, a web page including two tables, looking like (one for the modified query, one for the classic query) should appear. 


\subsection{Retrieving Documents using the Speedy Query}
To retrieve documents similar according to the similarity score defined in section \ref{sec:kld}, but also using the filtering approach described in section \ref{sec:speedy} the endpoint \textit{mlt/speedy/id} is used.
This will send a function score query as shown in section \ref{sec:speedy}.
In the unlikely case that no documents satisfy the constraints enforced by the range query, the modified query discussed in section \ref{sec:modified} is sent. 
\\\\
An example query (using curl) could look like this:\\
\textit{curl -XGET http://localhost:5000/mlt/speedy/123}

\subsection{Indexing a new Document}
\label{sec:indexusage}
The ability to index a new document is provided by the endpoint \textit{mlt/add}.
This expects a POST request, transmitting a json file which needs to have at least the field \textit{text}. The topic distribution of the text inside this field will be calculated and the information will be added to the document before the document is indexed in elasticsearch. If data that does not correspond to a valid json is posted, or a json document which does not contain a field called \textit{text}, a response with error code 400 is returned.
\\\\
An example query (using curl) could look like this:\\
\textit{curl -XPOST http://localhost:5000/mlt/add -d @myjson.json -H 'Content-Type: application/json'}

\subsection{Running the tests}
All tests and their documentation are found in the \textit{testcases.py} file.
The tests can be run by issuing the command \textit{pytest testcases.py}. 

\section{Documentation}
All documentation of the code was generated using the \textit{pdoc} library. Files containing the documentation of the server, the script training the topic model for the initial indexing and the tests can be found in the \textit{documentation} directory.

\section{Conclusion}
A comparison of the proposed approach to the classic MLT approach yields interesting results. An objective measure of the quality of the proposals is not possible, since both approaches use fundamentally different similarity scores. It can however be concluded that the recommendations by the proposed method are broader in scope and can therefore be successfully used in applications as outlined in the introductory section of this document.
In some cases the more general suggestions made by the proposed approach are worse. Consider the example document \textit{Logical Equivalence} which can be found by querying the \textit{/mlt/compare/300} endpoint.
Here the classic approach suggests articles related to logic (\textit{negation, eclusive or, etc.}) while the modified approach suggests general articles about mathematics. 

In other use cases however the modified approach shines. When querying the document \textit{Symphony No. 4 (Mahler)} available under \textit{/mlt/compare/207}, the classic approach exclusively suggests articles about other symphonies by Mahler. The modified approach however takes a broader view and suggests articles more generally about music, such as an article about \textit{Concerto grosso}.
It can therefore be said that the choice of the approach strongly dependent on the use case.  


\begin{thebibliography}{99}
\bibitem[MLT]{MLT} https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-mlt-query.html
\bibitem[LDAP]{LDAP} "Plate notation of the Smoothed LDA Model" by Wikipedia User Slxu.public, licensed under CC BY-SA 3.0
\bibitem[WEX]{WEX} "Wikipedia Extractor" http://medialab.di.unipi.it/wiki/Wikipedia\_Extractor
\bibitem[ESMAP]{ESMAP}https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-script-fields.html
\bibitem[FSSS]{FSSS}https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-function-score-query.html
\bibitem[SKL]{SKL}https://scikit-learn.org/stable/documentation.html
\end{thebibliography}
\newpage
\appendix
\section{Appendix}
\label{appendix}
The plots below show for each topic: 
the percentage of documents this topic contributes to more than x percent 

\setboolean{@twoside}{false}
\includepdf[pages=-, offset=5 -5]{excluded_docs_by_threshold_0_5.pdf}
\includepdf[pages=-, offset=5 -5]{excluded_docs_by_threshold_6_11.pdf}
\includepdf[pages=-, offset=5 -5]{excluded_docs_by_threshold_12_17.pdf}
\includepdf[pages=-, offset=5 -5]{excluded_docs_by_threshold_18_23.pdf}
\includepdf[pages=-, offset=5 -5]{excluded_docs_by_threshold_24_29.pdf}


\end{document}

