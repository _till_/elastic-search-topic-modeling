# Elastic Search Topic Modeling

Code for my student project for the _Elastic Search_ course at Potsdam University offered in winter term 2018/2019.

## Goal

The goal of this project is to implement a solution akin to the _More Like This_ feature offered by Elastic Search but using a similarity measure based on topic distributions of documents in the index.

In a first stage a topic model will be created on the documents in an index. When adding a new document to the index we will calculate the topic distribution of the document according to the topic model. When searching for more documents like this the topic distributions will be compared and documents with similar topic distributions will be suggested.

Suggested documents will be compared to documents suggested by the standard More Like This function.

## Components

1. A _flask_ server which provides the functionalities in the form of a REST API is found in the _server.py_ file.
2. The buildscript _build.sh_ will start docker containers running elasticsearch and cerebro, index the documents and start up the flask server
3. A python script used for the initial training of the topic model and saving all the prepared documents is provided in the _prepare_initial_index.py_ file.
This script does NOT have to be run for the project to work. The results of the script are already saved and provided in the _.pkl_ files. It is merely intended as a proof of work.
4. Documentation of the code can be found in the _documentation_ directory
5. A project report, detailing thoughts and theory behind the used approach can be found in the _writeup_ directory
This report includes descriptions of the provided functionalities and endpoints provided by the flask server, so it is a good start to read this first.
6. Test cases are containend in the _testcases.py_ file. The tests can be run issuing the command _pytest testcases.py_
7. The directories _static_ and _templates_ are used for the compare view of the flask server
8. The _exploratory_ directory contains a jupyter notebook used for creating the plots in the writeup.
9. The examples querries outlined in the writeup are saved in the _example-querries_ directory for convenience

 ## How to Start?
 A good place to start is reading the project report situated in the writeup directory.
 The project itself can be built in the _build_ directory by running the buildscript. I recommend leaving the default host and port configurations for elasticsearch, but in case it is needed they can be changed in the constants at the top of the files (buildscript, dockerfile, server.py and testcases.py).
 After running the build script the flask server should start automatically. During the build process a virtual environment will be created. To run the tests it suffices to activate the virtual environment and run _pytest testcases.py_

 After running the tests I recommend using the compare view described in the writeup to get a feel for the differences in the the suggestions made by both approaches.
