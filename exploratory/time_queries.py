import requests
import time
import json
from tqdm import tqdm

# number of requests to make and everage over
N=1000
# url of elastic search
ES_URL= "http://localhost:9200"
# name of elastic search index
ES_INDEX="topic_docs"
# url where flask is running
FLASK_URL="http://localhost:5000"

def measure_speeds():
    """ Measures the speed of different queries.

    This sends `N` modified more like this queries and `N` modified queries using prefiltering
    to elasticsearch and returns the average time it took for both query times.
    This reports the average value of `took` field in the elastic search response to the 
    queries. The took field reports the time it took elasticsearch to process the query.

    """
    speedy_times = list()
    no_filter_times = list()
    for i in tqdm(range(N)):

        # modified query without filtering
        modified_query = json.loads(requests.get(FLASK_URL + "/mlt/modified/{}".format(i)).text)
        no_filter_times.append(modified_query["took"])
        clear_cache = requests.post("/".join([ES_URL, ES_INDEX, "_cache/clear"]))

        # speedy query
        speedy_query = json.loads(requests.get(FLASK_URL  + "/mlt/speedy/{}".format(i)).text)
        speedy_times.append(speedy_query["took"])
        clear_cache = requests.post("/".join([ES_URL, ES_INDEX, "_cache/clear"]))

    slow_time = sum(no_filter_times) / len(no_filter_times)
    speedy_time = sum(speedy_times) / len(speedy_times)
    print("Modified Query without prefiltering took on average {}ms\nSpeedy Query took on average {}ms".format(
                                                                                    slow_time, speedy_time))
    return(slow_time, speedy_time)

if __name__ == "__main__":
    measure_speeds()
