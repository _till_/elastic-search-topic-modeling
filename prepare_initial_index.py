import os
import json
from sklearn.externals import joblib
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation

ARTICLE_MAIN_DIR = "all_texts/"
MIN_LENGTH = 250
ES_INDEX = "topic_docs"
ES_TYPE = "article"

def prepare_articles():
    """Load articles from the wikidump.
    
    This traverses all directories with articles extracted using the 
    wikiextractor. It loads all articles and their meta data into lists
    provided they have a minimum length (default 250 words).
    Filtering out very short articles is beneficial for the topic modeling
    process."""
    # contain raw articles / articles with metadata as json respectively
    articles, articles_meta = list(), list()
    for directory in os.listdir(ARTICLE_MAIN_DIR):
        for _file in os.listdir(ARTICLE_MAIN_DIR + directory): 
            with open("{}{}/{}".format(ARTICLE_MAIN_DIR, directory, _file)) as article_file:
                for line in article_file.readlines():
                    article = json.loads(line)
                    # only use articles that have MIN_LENGTH nr. of words or more
                    if(len(article["text"].split()) > MIN_LENGTH):
                        articles_meta.append(article)
                        articles.append(article["text"])
    return articles, articles_meta


def get_term_frequencies(articles):
    """Transform a list of articles into a document-termfrequency matrix.
    
    Each articles is split into words. Numbers, words with less than 3 characters,
    words appearing only once in the corpus and words appearing in more than 90 percent of 
    articles are disregarded. Stopwords contained in the standard stopword list for english 
    provided by sklearn are filtered out as well. 
    The 10000 most frequent of the remaining words are kept."""
    tf_vectorizer = CountVectorizer(max_df=0.90, 
                                token_pattern='[a-zA-Z]{3,}',
                                min_df=2, max_features=10000, stop_words='english')
    tf = tf_vectorizer.fit_transform(articles)
    return tf, tf_vectorizer.get_feature_names()


def run_lda(term_frequencies, n_topics=25):
    """Run the lda algorithm on all available cores. 

    Keyword Arguments:  
    `term_frequencies`: the document-term-frequency matrix  
    `n_topics`: the number of topics (default = 25)
    """
    lda = LatentDirichletAllocation(n_components=n_topics, max_iter=5, n_jobs = -1, 
                                learning_method='online', random_state=0).fit(term_frequencies)
    return lda    

        
def display_topics(model, features, no_top_words):
    """Print the most significant words of each of the found topics.  
    
    Keyword Arguments:  
    `model`: the trained lda model    
    `features`: the words corresponding to features  
    `no_top_words`: the number of words to print for each topic
    """
    for topic_idx, topic in enumerate(model.components_):
        print("Topic {}:".format(topic_idx))
        print(" ".join([features[i] for i in topic.argsort()[:-no_top_words - 1:-1]]))


def prepare_to_index(lda, tfs, articles_meta):
    """ Calculate topic distribution for each article and add that information.
    Returns a list of the articles, enriched with topic information and elasticsearch
    attributes.

    Takes a list of articles and their meta data in json format, calculates the topic
    distribution of the article and adds that information to the articles json.
    Additionaly the elasticsearch index, document type and id are added.

    Keyword Arguments:
    `lda`: the lda model used to transform the articles
    `tfs`: the term-frequencies of the provided articles
    `articles_meta`: the articles for which to compute the topic distribution
    """
    to_index = articles_meta
    for idx, doc in enumerate(tfs):
        t_dist = lda.transform(doc)
        for dist_key, dist_v in enumerate(t_dist.flatten()):
            to_index[idx]["topic_{}".format(dist_key)] = dist_v
        to_index[idx]["_id"] = str(idx)
        to_index[idx]["_index"] = ES_INDEX
        to_index[idx]["_type"] =  ES_TYPE
    return(to_index)


def prepare_initial_index():
    """ Load 50000 articles from the wikipedia dump, train an lda model on them and save
    everything needed to index the articles.

    Trains the LDA-Model and saves the model and vocabulary so that new documents can be
    indexed with those files. Also adds topic information to the initial corpus und saves 
    the enriched list of articles that is ready to index in elasticsearch.
    
    """
    articles, articles_meta = prepare_articles()
    # use the first 50000 articles
    articles = articles[:50000]
    articles_meta = articles_meta[:50000]
    tfs, feature_names = get_term_frequencies(articles)
    joblib.dump(feature_names, "vocabulary.pkl")
    lda = run_lda(tfs, 25)
    display_topics(lda, feature_names, 10)
    joblib.dump(lda, "mylda.pkl")
    to_index = prepare_to_index(lda, tfs, articles_meta)
    with open("to_index.pkl", "wb") as ti:
        joblib.dump(to_index, ti)

if __name__ == "__main__":
    prepare_initial_index()
