from flask import Flask, request, render_template, Response
from collections import defaultdict
import requests
import json
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.externals import joblib
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd
import numpy as np
from elasticsearch import Elasticsearch

"""
Code for the REST API of the project. 

The server will provide at least the following endpoints: 
    /mlt/classic/<id> : returns the documents considered similar by original MLT to the document with specified id
    /mlt/modified/<id> : returns the documents considered similar by the topic based similarity measure to the document with specified id
    /mlt/speedy/<id> : like modified, but with filtering hopeless candidates
    /mlt/add : a route that let's user post a document, calculates the topic distribution and indexes this document
    /mlt/compare/<id> : renders a view showing results of the classic and modified approaches
"""


app = Flask(__name__)
N_TOPICS = 25
ES_HOST = "localhost"
ES_PORT = 9200
ES_URL = "http://" + ES_HOST + ":" + str(ES_PORT)
INDEX_NAME = "topic_docs"
ES_DOC_TYPE = "article"

# preload topic model, for indexing new documents
mlda = joblib.load("mylda.pkl")
vocab = joblib.load("vocabulary.pkl")
tf_vectorizer = CountVectorizer(max_df=0.95, min_df=2, vocabulary=vocab, stop_words="english")




@app.route("/mlt/classic/<id>")
def classic(id, index_url="/".join([ES_URL, INDEX_NAME, "_search"]), ):
    """Return documents considered similar by original MLT.
    
    This is a wrapper around the original More Like This query provided
    by elastic search. After building a query in json format for the specified
    id, the query gets send to the ES server and the resulting is returned as
    a string.  

    Keyword Arguments:  
    `id` : the elastic search id of the document of interest
    """
    query = build_query(id=id)
    head = {"Content-type": "application/json"}
    print(query)
    r = requests.get(index_url, data=query, headers=head)
    # post process the result
    similar_docs = json.loads(r.text)
    for d in similar_docs["hits"]["hits"]:
        print(d["_id"], " : ", d["_source"]["title"]) 
    return r.text


@app.route("/mlt/modified/<id>")
def modified(id, es_url=ES_URL, index=INDEX_NAME, _type=ES_DOC_TYPE):
    """ Return documents with a similar topic distribution to the document of interest.  
    
    After getting the topic distribution of the queried document, a scripted query is built
    and sent to the elastic search server.  

    Keyword Arguments:  
    `id` : the elastic search id of the document of interest
    """
    t_dist = get_topic_dist(es_url, index, _type, id)
    # build and send query to get documents with similar topic distributions
    query = build_modified_query(t_dist)
    print(query)
    head = {"Content-type": "application/json"}
    r = requests.post("/".join([es_url, index, _type, "_search"]), data=query, headers=head) 
    # post process the result
    similar_docs = json.loads(r.text)
    print(similar_docs)
    for d in similar_docs["hits"]["hits"]:
        print(d["_id"], " : ", d["_source"]["title"]) 
    return r.text


@app.route("/mlt/speedy/<id>")
def speedy(id, es_url=ES_URL, index=INDEX_NAME, _type=ES_DOC_TYPE, thresh=0.05):
    """Return documents with a topic distribution similar to the one of document with id `id`,
    but use prefiltering to speed up the process.
    
    After getting the topic distribution of the document of interest, this information is used
    to build a `speedy_query` - a query which discards all documents with a topic distribution
    where this topic does not have an influence larger than the given threshold.
    All documents containing the most significant topic to an amount larger than `thresh` are
    returned, ordered by their similarity to the document with id `id`.

    Keyword Arguments:  
    `id`: the id of the queried document  
    `es_url`: the url of the elastic search server  
    `index`: the name of the elastic search index  
    `type`: the name of the elastic search type  
    `thresh`: threshold - all documents containing less than the specified amount of the most  
    significant topic of the queried document will be disregarded without further checking for  
    similarity
    """
    t_dist = get_topic_dist(es_url, index, _type, id)
    query = build_speedy_query(t_dist=t_dist, thresh=thresh)
    print(query)
    head = {"Content-type": "application/json"}
    r = requests.post("/".join([es_url, index, _type, "_search"]), data=query, headers=head) 
    # post process the result
    similar_docs = json.loads(r.text)
    print(similar_docs)
    # backup plan: carry out modified query without prefiltering
    if len(similar_docs["hits"]["hits"]) < 5:
        print("Sped up query did not return enough documents. Using modified query...")
        return modified(id=id)
    for d in similar_docs["hits"]["hits"]:
        print(d["_id"], " : ", d["_source"]["title"]) 
    return r.text


@app.route("/mlt/compare/<id>")
def compare(id, es_url=ES_URL, index=INDEX_NAME, _type=ES_DOC_TYPE):
    """Render a view that compares the results of the original/modified queries in tabular form.  
    
    This route provides a comparison of the results of the standard/modified MLT queries in
    tabular form. A standard MLT query and a modified query for the document with the
    given `id` are sent to the elastic search server. The 5 best matching documents according to each query
    are presented.
    """
    orig, mod = classic(id), modified(id)
    orig_sim = json.loads(orig)
    orig_list = list()
    for d in orig_sim["hits"]["hits"]:
        orig_list.append((d["_id"], d["_source"]["title"], d["_source"]["text"]))   
    # by default only max. 50 characters are returned per cell; make it return more
    pd.set_option('display.max_colwidth', 1000)
    orig_table = pd.DataFrame.from_records(orig_list[:5], 
                              columns=["id", "title", "text"]).to_html(index=False)
    print("Orig table created")
    mod_sim = json.loads(mod)
    mod_list = list()
    # the first result will be the document itself, filter this out
    for d in mod_sim["hits"]["hits"][1:]:
        mod_list.append((d["_id"], d["_source"]["title"], d["_source"]["text"]))
    mod_table = pd.DataFrame.from_records(mod_list[:5], 
                             columns=["id", "title", "text"]).to_html(index=False)
    head = {"Content-type": "application/json"}
    r = requests.get("/".join([es_url, index, _type, id]), headers=head)
    doc_title = json.loads(r.text)["_source"]["title"] 
    return render_template("compare_view.html",tables=[orig_table, mod_table], 
			query_doc=doc_title, 
			titles=["y1based", "Original Similar Articles", "Modified Similar Articles"])


@app.route("/mlt/add", methods=["POST"])
def index_document():
    """Index a new document.
    
    This route can be used to post a document that will be enriched with topic information and then
    indexed in elastic search. It expects a valid `json` with at least a field `text` which 
    contains the text used to compute the topic distribution. 
    """
    article = request.json
    if "text" not in article.keys():
        return "A 'text' field is needed!", 400
    content = annotate_document(article)
    es = Elasticsearch(hosts=[{"host": ES_HOST, "port": ES_PORT}])
    res = es.index(index=INDEX_NAME, doc_type=ES_DOC_TYPE, body=content)
    indexed_id = res["_id"]
    return(indexed_id)


def annotate_document(doc):
    """Calculate the topic distribution of a document and add
    this information to the provided json.  

    The pretrained topic model saved in `mlda` is used to compute the topic distribution of the provided
    document. This information is added to the json and the modified json document is returned.  

    Keyword Arguments:  
    `doc`: the json document to annotate with topic information. It needs to have at least 
    a field `text`, which contains the text of which the topic distribution is calculated.

    """
    content = doc
    text = tf_vectorizer.fit_transform(list(content["text"]))
    t_dist = mlda.transform(text[0])
    for dist_key, dist_v in enumerate(t_dist.flatten()):
        content["topic_{}".format(dist_key)] = dist_v
    return content 


def build_modified_query(dist):
    """Return a query that can be sent to elastic search to get back documents with a
    similar topic distribution to the one provided.  

    This builds a scripted query. The query calculates the inverse of the 
    Kullback Leibler Divergence of the specified topic distribution with the topic distributions
    of all other documents in the elastic search index. 
    The Kullback Leibler Divergence measures, how different one probability distribution is to
    another, so the inverse can be used to find similar documents.

    Keyword Arguments:  
    `dist`: the topic distribution
    """
    params = {"topic_{}".format(i) : val for i, val in enumerate(dist)}
    sources = ["params.topic_{} * Math.log(params.topic_{} / doc['topic_{}'].value)".format(i,i,i)
                for i in range(len(dist))]
    script_formula = "return 1/(" + '+'.join(sources) + " + 0.1);"
    query = defaultdict(lambda: defaultdict(dict))
    query["function_score"]["query"] = dict()
    query["function_score"]["query"]["match_all"] = {}
    query["function_score"]["script_score"] = defaultdict(lambda:defaultdict(dict))
    for pkey in params.keys():
        query["function_score"]["script_score"]["script"]["params"][pkey] = params[pkey]
    query["function_score"]["script_score"]["script"]["source"] = script_formula
    query["function_score"]["boost_mode"] = "replace"
    query = {"query" : query}
    return(json.dumps(query))


def build_speedy_query(t_dist, thresh=0.05):
    """
    Return a modified query that first filters out all documents with a small part of the most
    significant topic.  

    This builds a function score query which calculates the inverse of the KL-divergence for 
    a subset of documents in the elastic search index. This is done by applying a range 
    query before the actual scripted query is applied. The range query filters out all 
    documents in which the most significant topic in `t_dist` does not have an impact above
    the amount specified by `thresh`.

    Keyword Arguments:  
    `t_dist`: topic distribution for which similar documents are queried
    `thresh`: documents containing an amount less than `thresh` of the most significant 
    topic in `t_dist` are disgarded, before the scripted score is calculated for them.
    This is done in order to improve the execution speed of the query.
    """
    max_topic = np.argmax(t_dist)
    params = {"topic_{}".format(i) : val for i, val in enumerate(t_dist)}
    sources = ["params.topic_{} * Math.log(params.topic_{} / doc['topic_{}'].value)".format(i,i,i)
                for i in range(len(t_dist))]
    script_formula = "return 1/(" + '+'.join(sources) + " + 0.1);"
    query = defaultdict(lambda: defaultdict(dict))
    query["function_score"]["query"] = dict()

    # range query to filter out docs with low contribution of max_topic
    query["function_score"]["query"]["range"] = dict()
    query["function_score"]["query"]["range"]["topic_{}".format(max_topic)] = dict()
    # filter out everything with lower contribution than thresh
    query["function_score"]["query"]["range"]["topic_{}".format(max_topic)]["gte"] = thresh

    # the actual script
    query["function_score"]["script_score"] = defaultdict(lambda:defaultdict(dict))
    for pkey in params.keys():
        query["function_score"]["script_score"]["script"]["params"][pkey] = params[pkey]
    query["function_score"]["script_score"]["script"]["source"] = script_formula
    query["function_score"]["boost_mode"] = "replace"
    query = {"query" : query}
    return(json.dumps(query))


def build_query(field="text", index=INDEX_NAME, _type=ES_DOC_TYPE, 
			id=42, min_term_freq=2, max_query_terms=25):
    """Return a json containing a standard MLT query for a specified document.

    Keyword arguments:  
    `field`: field on which to perform the MLT query (default "text")  
    `index`: elastic search index to query (default "topic_docs")  
    `_type`: elastic search index type to query (default "article")  
    `_id`:   elastic search id of the document to query (default 42)  
    `min_term_freq`: minimum term frequency below which the terms will be
                     ignored from the input document (default 2)  
    `max_query_terms`: maximum number of query terms that will be selected (default 25)  
    """
    qdict = defaultdict(lambda: defaultdict(dict))
    qdict["query"]["more_like_this"]["fields"] = [field]
    qdict["query"]["more_like_this"]["like"] = [{"_index" : index, 
						 "_type" : _type, "_id" : str(id)}]
    qdict["query"]["more_like_this"]["min_term_freq"] = min_term_freq
    qdict["query"]["more_like_this"]["max_query_terms"] = max_query_terms
    return json.dumps(qdict)


def get_topic_dist(es_url, index, _type, id):
    """Return the topic distribution of a given document.  

    This sends a request to the elastic search server, asking for the document with the
    specified `id` and extracts the topic distribution of the document.  

    Keyword Arguments:  
    `id`: the elastic search id of the queried document
    """
    # get the document by id
    doc = requests.get("/".join([es_url, index, _type, id]))
    doc = json.loads(doc.text)
    # read the topic distribution
    t_dist = list()
    for i in range(N_TOPICS):
        t_dist.append(doc["_source"]["topic_{}".format(i)])
    return t_dist

