ELASTIC_URL='http://localhost:9200'
INDEX_NAME='topic_docs'
VENV='myesenv3'


echo "Starting Elastic Search"
docker-compose up -d

echo "Waiting for Elastic Search to start..."
while ! curl $ELASTIC_URL; do sleep 5; done
 
echo "Delete index..."
curl -XDELETE $ELASTIC_URL/$INDEX_NAME/

echo "Put index..."
curl -XPUT $ELASTIC_URL/$INDEX_NAME/ -d @index_settings.json -H 'Content-Type: application/json'

echo "Post mappings..."
curl -XPOST $ELASTIC_URL/$INDEX_NAME/article/_mapping -d @article_mapping.json -H 'Content-Type: application/json'

echo "Prepare python virtual environment..." 
virtualenv -p python3 $VENV
source $VENV/bin/activate
pip install -r requirements.txt

echo "Index documents..."
python index_documents.py

echo "Starting the falsk server..."
export FLASK_APP=../server.py
flask run
 
