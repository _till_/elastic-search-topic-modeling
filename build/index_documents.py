import os
from sklearn.externals import joblib
from elasticsearch import Elasticsearch, helpers

ES_HOST = "localhost"
ES_PORT = 9200

with open("./to_index.pkl", "rb") as docs:
    to_index = joblib.load(docs)

es = Elasticsearch(hosts=[{"host": ES_HOST, "port": ES_PORT}])   
helpers.bulk(es, to_index)
