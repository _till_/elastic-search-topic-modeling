import server
import requests
import numpy as np
import json

ES_URL = "http://localhost"
ES_PORT = "9200"
ES_INDEX = "topic_docs"
ES_TYPE = "article"
FLASK_URL = "http://localhost:5000"
#np.random.seed(42)
def valid_json(j):
    try:
        json_object = json.loads(j)
    except ValueError:
        return False
    return True


def kl_div(p, q):
    """Calculate the kl divergence between distributions p and q"""
    return p @ np.log(p/q)


def sim_score(p, q):
    """Calculate the similarity between 2 documents with topic distributions p and q"""
    return 1 / (kl_div(p,q) + 0.1)


# test the setup
def test_es_running():
    """Asserts elasticsearch is running on the correct port"""
    r = requests.get(ES_URL + ":" + ES_PORT)
    assert json.loads(r.text)["tagline"] == "You Know, for Search"
    assert r.status_code == 200


def test_es_index_exists():
    """Was the index created successfully?"""
    r = requests.get(ES_URL + ":" + ES_PORT + "/" + ES_INDEX)
    assert r.status_code == 200


def test_es_mapping():
    """Does the mapping contain the topic information?"""
    r = requests.get(ES_URL + ":" + ES_PORT + "/" + ES_INDEX)
    assert r.status_code == 200
    for i in range(25):    
         assert "topic_{}".format(i) in json.loads(r.text)[ES_INDEX]["mappings"][ES_TYPE]["properties"].keys()


def test_articles_indexed():
    """Did all 50.000 articles get indexed?"""
    r = requests.get("/".join([ES_URL + ":" + ES_PORT, ES_INDEX, ES_TYPE, "_search"]))
    assert json.loads(r.text)["hits"]["total"] >= 50000


# test whether the build query commands build valid json
def test_classic_query_valid_json():
    query = server.build_query([0.3, 0.2, 0.5])
    assert valid_json(query)


def test_build_modified_valid_json():
    query = server.build_modified_query([0.3, 0.2, 0.5])
    assert valid_json(query)


def test_speedy_modified_valid_json():
    query = server.build_speedy_query([0.3, 0.2, 0.5])
    assert valid_json(query)


# test whether the returned documents have the score they should have via KL divergence
def test_score_calculation_modified():
    """Select a random document from the corpus, query for similar documents and test whether
       the scores look good for modified query.

    Checking whether the scores are correct up to 3 digits behind the comma 
    (round because of numerical issues when multiplying tiny numbers)
    """
    id = np.random.randint(50000)
    tdist_this = list()
    r = requests.get("/".join([ES_URL + ":" + ES_PORT, ES_INDEX, ES_TYPE, str(id)]))
    query_doc = json.loads(r.text)
    for i in range(25):
        tdist_this.append(query_doc["_source"]["topic_{}".format(i)])

    sim_docs = json.loads(server.modified(str(id)))
    for d in sim_docs["hits"]["hits"]:
        tdist_other = list()
        for i in range(25):
            tdist_other.append(d["_source"]["topic_{}".format(i)])
        assert np.round(sim_score(np.array(tdist_this), np.array(tdist_other)), 3) == np.round(d["_score"], 3)


def test_score_calculation_speedy():
    """Select a random document from the corpus, query for similar documents and test whether
       the scores look good for the speedy query.

    Checking whether the scores are correct up to 3 digits behind the comma 
    (round because of numerical issues when multiplying tiny numbers)
    """
    id = np.random.randint(50000)
    tdist_this = list()
    r = requests.get("/".join([ES_URL + ":" + ES_PORT, ES_INDEX, ES_TYPE, str(id)]))
    query_doc = json.loads(r.text)
    for i in range(25):
        tdist_this.append(query_doc["_source"]["topic_{}".format(i)])

    sim_docs = json.loads(server.speedy(str(id)))
    for d in sim_docs["hits"]["hits"]:
        tdist_other = list()
        for i in range(25):
            tdist_other.append(d["_source"]["topic_{}".format(i)])
        assert np.round(sim_score(np.array(tdist_this), np.array(tdist_other)), 3) == np.round(d["_score"], 3)


# does the orignial query return the same result as if we queried elastic search directly?
def test_original_query():
    id = np.random.randint(50000)
    classic_restult = server.classic(id)
    

# test whether the number one result with modified/speedy query is the same document
def test_top_result_modified():
    id = np.random.randint(50000)
    sim_docs = json.loads(server.modified(str(id)))
    assert sim_docs["hits"]["hits"][0]["_id"] == str(id)


def test_top_result_speedy():
    id = np.random.randint(50000)
    sim_docs = json.loads(server.speedy(str(id)))
    assert sim_docs["hits"]["hits"][0]["_id"] == str(id)


# test whether we can index a document
valencia = """Valencia (/vəˈlɛnsiə/; Spanish: [baˈlenθja]), officially València (Valencian: [vaˈlensia]),[3] on the east coast of Spain, is the capital of the autonomous community of Valencia and the third-largest city in Spain after Madrid and Barcelona, with around 800,000 inhabitants in the administrative centre. Its urban area extends beyond the administrative city limits with a population of around 1.6 million people.[2][4] Valencia is Spain's third largest metropolitan area, with a population ranging from 1.7 to 2.5 million[1] depending on how the metropolitan area is defined. The Port of Valencia is the 5th busiest container port in Europe and the busiest container port on the Mediterranean Sea. The city is ranked at Beta-global city in the Globalization and World Cities Research Network.[5] Valencia is integrated into an industrial area on the Costa del Azahar (Orange Blossom Coast).

Valencia was founded as a Roman colony by the consul Decimus Junius Brutus Callaicus in 138 BC, and called Valentia Edetanorum. In 714 Moroccan and Arab Moors occupied the city, introducing their language, religion and customs; they implemented improved irrigation systems and the cultivation of new crops as well. Valencia was the capital of the Taifa of Valencia. In 1238 the Christian king James I of Aragon conquered the city and divided the land among the nobles who helped him conquer it, as witnessed in the Llibre del Repartiment. He also created a new law for the city, the Furs of Valencia, which were extended to the rest of the Kingdom of Valencia. In the 18th century Philip V of Spain abolished the privileges as punishment to the kingdom of Valencia for aligning with the Habsburg side in the War of the Spanish Succession. Valencia was the capital of Spain when Joseph Bonaparte moved the Court there in the summer of 1812. It also served as capital between 1936 and 1937, during the Second Spanish Republic.
"""

def test_document_indexing():
    head = {"Content-type": "application/json"}
    doc = {"text": valencia, "title": "Valencia"}
    r = requests.post("/".join([FLASK_URL, "mlt/add"]), data=json.dumps(doc), headers=head)
    indexed_id = r.text
    r = requests.get("/".join([ES_URL + ":" + ES_PORT, ES_INDEX, ES_TYPE, indexed_id]))
    query_doc = json.loads(r.text)
    assert query_doc["_source"]["title"] == "Valencia"


def test_added_document_contains_topics():
    head = {"Content-type": "application/json"}
    doc = {"text": valencia, "title": "Valencia"}
    r = requests.post("/".join([FLASK_URL, "mlt/add"]), data=json.dumps(doc), headers=head)
    indexed_id = r.text
    r = requests.get("/".join([ES_URL + ":" + ES_PORT, ES_INDEX, ES_TYPE, indexed_id]))
    query_doc = json.loads(r.text)
    for i in range(25):
        assert "topic_{}".format(i) in query_doc["_source"].keys()


def test_added_document_topics_greater_0():
    head = {"Content-type": "application/json"}
    doc = {"text": valencia, "title": "Valencia"}
    r = requests.post("/".join([FLASK_URL, "mlt/add"]), data=json.dumps(doc), headers=head)
    indexed_id = r.text
    r = requests.get("/".join([ES_URL + ":" + ES_PORT, ES_INDEX, ES_TYPE, indexed_id]))
    query_doc = json.loads(r.text)
    for i in range(25):
        assert query_doc["_source"]["topic_{}".format(i)] > 0


def test_adding_invalid_json():
    """Test whether the correct status code (400) is returned when trying 
    to index somtehing other than a json
    """
    head = {"Content-type": "application/json"}
    r = requests.post("/".join([FLASK_URL, "mlt/add"]), data=valencia.encode("utf-8"), headers=head)
    indexed_id = r.text
    print(indexed_id)
    r = requests.get("/".join([ES_URL + ":" + ES_PORT, ES_INDEX, ES_TYPE, indexed_id]))
    assert r.status_code == 400


def test_adding_json_without_text_field():
    """Assert that a 400 is thrown if no text field is provided, and that a 
    helpfull error message is sent
    """
    head = {"Content-type": "application/json"}
    doc = {"ttext": valencia, "title": "Valencia"}
    r = requests.post("/".join([FLASK_URL, "mlt/add"]), data=json.dumps(doc), headers=head)
    assert r.status_code == 400
    assert r.text == "A 'text' field is needed!"


